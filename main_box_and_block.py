from tkinter import *
from tkinter.ttk import Combobox
from tkinter.ttk import Separator

import utils
import box_and_block


def createUI():
    def start_box_and_block():
        camera_id = cb1.get()
        hand = cb2.get()
        t = scale_tolerance.get()
        hand = 'R' if hand == "Right" else 'L'
        if camera_id == "No cameras available!":
            exit(1)
        else:
            window.destroy()
            box_and_block.start(int(camera_id[-1]), hand, t)

    window = Tk()

    handeness = ("Left", "Right")
    cameraIndexes = utils.returnCameraIndexes()
    if len(cameraIndexes) == 0:
        cameraIndexes.append("No cameras available!")
    l_camera = Label(window, text="Camera")
    cb1 = Combobox(window, values=cameraIndexes, state="readonly")
    cb1.current(len(cameraIndexes) - 1)

    l_hand = Label(window, text="Hand")
    cb2 = Combobox(window, values=handeness, state="readonly")
    cb2.current(1)

    scale_tolerance = Scale(window, from_=1.0, to=4.5, length=200, resolution=0.1, orient=HORIZONTAL)
    l_tolerance = Label(window, text="Exercise Tolerance")

    button_start = Button(window, height=2, width=20, text="Start!", command=lambda: start_box_and_block())
    separator1 = Separator(window, orient='horizontal')
    separator2 = Separator(window, orient='horizontal')
    separator3 = Separator(window, orient='horizontal')

    separator1.pack(padx=10, pady=5, fill='x')
    l_camera.pack(pady=5)
    cb1.pack()

    l_hand.pack(pady=5)
    cb2.pack()
    separator2.pack(padx=10, pady=10, fill='x')

    l_tolerance.pack(pady=5)
    scale_tolerance.set(2.5)
    scale_tolerance.pack()
    separator3.pack(padx=10, pady=10, fill='x')

    button_start.pack()
    window.title('Box & Block Options')
    window.geometry("450x340")
    window.resizable(False, False)
    window.mainloop()


if __name__ == "__main__":
    createUI()
