from tkinter import *
from tkinter.ttk import Combobox
from tkinter.ttk import Separator

import utils
import sollerman_wallet

def createUI():
    def start_sollerman_wallet():
        camera_id = cb1.get()
        t = scale_tolerance.get()
        if camera_id == "No cameras available!":
            exit(1)
        else:
            window.destroy()
            sollerman_wallet.start(int(camera_id[-1]), t)

    window=Tk()

    cameraIndexes = utils.returnCameraIndexes()
    if len(cameraIndexes) == 0:
        cameraIndexes.append("No cameras available!")
    l_camera = Label(window, text="Camera")
    cb1 = Combobox(window, values=cameraIndexes, state= "readonly")
    cb1.current(len(cameraIndexes)-1)

    scale_tolerance = Scale(window, from_=1.0, to=4.5, length=200, resolution=0.1, orient=HORIZONTAL)
    l_tolerance = Label(window, text="Exercise Tolerance")


    button_start = Button(window, height=2, width=20, text="Start!", command = lambda:start_sollerman_wallet())
    separator1 = Separator(window, orient='horizontal')
    separator2 = Separator(window, orient='horizontal')
    separator3 = Separator(window, orient='horizontal')

    separator1.pack(padx= 10, pady=5, fill='x')
    l_camera.pack(pady=5)
    cb1.pack()
    separator2.pack(padx= 10, pady=10, fill='x')

    l_tolerance.pack(pady=5)
    scale_tolerance.set(1.5)
    scale_tolerance.pack()
    separator3.pack(padx=10, pady=10, fill='x')

    button_start.pack()
    window.title('Sollerman - Wallet Options')
    window.geometry("450x280")
    window.resizable(False, False)
    window.mainloop()

if __name__ == "__main__":
    createUI()