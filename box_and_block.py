import sys

import cv2
import numpy as np
import time
import random
import csv
import os

from HandTrackingModule import HandDetector
import utils

# CSV ##
output_csv = open('output/box_and_block_out.csv', 'a+')
headers = ['Hand (Left/Right)', 'Score', 'Tolerance']
writer = csv.DictWriter(output_csv, delimiter=',', lineterminator='\n', fieldnames=headers)
if output_csv.tell() == 0:
    writer.writeheader()
writeResults = True
########
WIDTH = 1280
HEIGHT = 720
HAND = "R"
TOTAL_TIME = 63  # seconds
CLOSED_LENGTH = 80  # distance between thumb and index
NUM_VISIBLE_MOVABLE_BLOCKS = 3
MAX_VISIBLE_NON_MOVABLE_BLOCKS = 8
TOLERANCE = 3

colorR = (0, 0, 255)
colorG = (0, 255, 0)
colorB = (255, 0, 0)
colorY = (0, 255, 255)

x1, y1 = WIDTH // 2, HEIGHT // 2 + 80
x2, y2 = WIDTH // 2, HEIGHT

INITIAL_Y_POSITION = HEIGHT - 50

line_thickness = 7
score = 0

totalTime = TOTAL_TIME

movableRectList = []
nonMovableRectList = []

colorList = [colorR, colorG, colorB, colorY]

detector = HandDetector(maxHands=1)
end_test_time = 0


class DragRect:
    def __init__(self, posCenter, size=None):
        if size is None:
            size = [100, 100]
        self.posCenter = posCenter
        self.initialPosX = posCenter[0]
        self.initialPosY = posCenter[1]
        self.size = size
        self.color = random.choice(colorList)

    def update(self, cursor):
        self.posCenter = cursor[0], cursor[1] + 30

    def cursorWithinRegion(self, cursor):
        ox, oy = self.posCenter
        h, w = self.size

        if ox - TOLERANCE * w // 2 < cursor[0] < ox + TOLERANCE * w // 2 and \
                oy - TOLERANCE * h // 2 < cursor[1] < oy + TOLERANCE * h // 2:
            return True

        return False

    def getColor(self):
        return self.color

    def getInitialPosX(self):
        if HAND == 'L':
            return self.initialPosX
        else:
            return WIDTH - self.initialPosX

    def getInitialPosY(self):
        return self.initialPosY


def addRect(removeRect=None, keepRemovedRect=False, x_pos=80, y_pos=INITIAL_Y_POSITION):
    if removeRect is not None:
        movableRectList.remove(removeRect)
        if keepRemovedRect:
            if len(nonMovableRectList) < MAX_VISIBLE_NON_MOVABLE_BLOCKS:
                nonMovableRectList.append(removeRect)
            else:
                nonMovableRectList[:MAX_VISIBLE_NON_MOVABLE_BLOCKS // 2] = []
                nonMovableRectList.append(removeRect)
        if HAND == 'L':
            movableRectList.append(DragRect([x_pos, y_pos]))
        else:
            movableRectList.append(DragRect([WIDTH - x_pos, y_pos]))
    else:
        if HAND == 'L':
            movableRectList.append(DragRect([x_pos, y_pos]))
        else:
            movableRectList.append(DragRect([WIDTH - x_pos, y_pos]))


def start(camera_id, hand, tolerance):
    global totalTime, writeResults, score, HAND, end_test_time, TOLERANCE
    HAND = hand
    TOLERANCE = tolerance

    if os.name == 'nt':
        cap = cv2.VideoCapture(camera_id, cv2.CAP_DSHOW)
    else:
        cap = cv2.VideoCapture(camera_id, cv2.CAP_V4L2)
    cap.set(3, WIDTH)
    cap.set(4, HEIGHT)
    cap.set(cv2.CAP_PROP_FPS, int(60))

    if NUM_VISIBLE_MOVABLE_BLOCKS < 1 or NUM_VISIBLE_MOVABLE_BLOCKS > 4:
        raise Exception("Too many or too few NUM_VISIBLE_MOVABLE_BLOCKS")
    if MAX_VISIBLE_NON_MOVABLE_BLOCKS < 1 or MAX_VISIBLE_NON_MOVABLE_BLOCKS > 15:
        raise Exception("Too many or too few MAX_VISIBLE_NON_MOVABLE_BLOCKS")

    for i in range(NUM_VISIBLE_MOVABLE_BLOCKS):
        if i % 2 != 0:
            addRect(x_pos=80 + i * 230, y_pos=INITIAL_Y_POSITION - 100)
        else:
            addRect(x_pos=80 + i * 230)
    triesToMoveBlock = False
    rect = None

    startTime = time.time()

    while True:
        timeRemain = int(totalTime - (time.time() - startTime))
        success, img = cap.read()
        img = cv2.flip(img, 1)

        if timeRemain <= 0:
            utils.putTextRect(img, "Test Over", [300, 300], colorR=(35, 35, 129),
                              scale=6, thickness=5, offset=20)
            utils.putTextRect(img, f'Score: {score}', [300, 450], colorR=(35, 35, 129),
                              scale=6, thickness=5, offset=20)
            if writeResults:
                end_test_time = time.time()
                writer.writerow({'Hand (Left/Right)': HAND, 'Score': score, 'Tolerance': TOLERANCE})
                writeResults = False

            if int(time.time() - end_test_time) >= 5:
                output_csv.close()
                cap.release()
                cv2.destroyAllWindows()
                sys.exit(0)
        else:
            hands, img = detector.findHands(img, flipType=False)
            cv2.line(img, (x1, y1), (x2, y2), (35, 35, 129), thickness=line_thickness)

            if hands:
                lmList = hands[0]['lmList']
                length, info = detector.findDistance(lmList[4], lmList[8])

                if rect is None:
                    rect = movableRectList[-1]

                if length < CLOSED_LENGTH:
                    cursor = lmList[8]  # index finger tip landmark

                    if not triesToMoveBlock:
                        for r in movableRectList:
                            if r.cursorWithinRegion(cursor):
                                rect = r
                                triesToMoveBlock = True
                                break
                        # triesToMoveBlock = rect.cursorWithinRegion(cursor)

                    if triesToMoveBlock:
                        rect.update(cursor)
                        if rect.posCenter[1] >= y1 - 40 \
                                and x1 - rect.size[0] // 2 <= rect.posCenter[0] <= x1 + rect.size[0] // 2:
                            addRect(removeRect=rect, keepRemovedRect=False, x_pos=rect.getInitialPosX(),
                                    y_pos=rect.getInitialPosY())
                            triesToMoveBlock = False
                else:  # elif bool(rectList):
                    if (HAND == 'L' and rect.posCenter[0] > x1 + rect.size[0] // 2) or (
                            HAND == 'R' and rect.posCenter[0] < x1 - rect.size[0] // 2):
                        score += 1
                        rect.posCenter = rect.posCenter[0], INITIAL_Y_POSITION
                        # rectList[-1].setNonMovable()
                        addRect(removeRect=rect, keepRemovedRect=True, x_pos=rect.getInitialPosX(),
                                y_pos=rect.getInitialPosY())
                        triesToMoveBlock = False
                        rect = None
                    elif triesToMoveBlock:
                        addRect(removeRect=rect, keepRemovedRect=False, x_pos=rect.getInitialPosX(),
                                y_pos=rect.getInitialPosY())
                        triesToMoveBlock = False
                        rect = None

            imgNew = np.zeros_like(img, np.uint8)

            for r in movableRectList:
                cx, cy = r.posCenter
                w, h = r.size
                cv2.rectangle(imgNew, (cx - w // 2, cy - h // 2),
                              (cx + w // 2, cy + h // 2), r.getColor(), cv2.FILLED)
                utils.cornerRect(imgNew, (cx - w // 2, cy - h // 2, w, h), 40, rt=0, colorC=(35, 35, 129))

            for r in nonMovableRectList:
                cx, cy = r.posCenter
                w, h = r.size
                cv2.rectangle(imgNew, (cx - w // 2, cy - h // 2),
                              (cx + w // 2, cy + h // 2), r.getColor(), cv2.FILLED)
                utils.cornerRect(imgNew, (cx - w // 2, cy - h // 2, w, h), 40, rt=0, colorC=(35, 35, 129))

            out = img.copy()
            alpha = 0.2
            mask = imgNew.astype(bool)
            out[mask] = cv2.addWeighted(img, alpha, imgNew, 1 - alpha, 0)[mask]

            utils.putTextRect(out, "Box & Block", [50, 60], colorR=(35, 35, 129),
                              scale=3, thickness=3, offset=10)

            utils.putTextRect(out, f"Hand: {HAND}", [235, 100], colorR=(35, 35, 129),
                              scale=2, thickness=2, offset=10)

            utils.putTextRect(out, f'Score: {score}', [50, 100], colorR=(35, 35, 129),
                              scale=2, thickness=2, offset=10)

            utils.putTextRect(out, f'Time Remaining: {timeRemain}', [50, 140], colorR=(35, 35, 129),
                              scale=2, thickness=2, offset=10)
            img = out

        cv2.imshow("Image", img)
        key = cv2.waitKey(1) & 0xFF

        if key == ord('r'):
            score = 0
            movableRectList.clear()
            nonMovableRectList.clear()
            for i in range(NUM_VISIBLE_MOVABLE_BLOCKS):
                if i % 2 != 0:
                    addRect(x_pos=80 + i * 230, y_pos=INITIAL_Y_POSITION - 100)
                else:
                    addRect(x_pos=80 + i * 230)
            rect = None
            startTime = time.time()
            totalTime = TOTAL_TIME
            writeResults = True
            triesToMoveBlock = False
        if key == ord('q'):
            break

    cv2.destroyAllWindows()
