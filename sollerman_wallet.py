import math
import cv2
import numpy as np
import time
import csv
import os
import sys

from HandTrackingModule import HandDetector
import utils

# CSV ##
output_csv = open('output/sollerman_wallet_out.csv', 'a+')
headers = ['Time', 'Score', 'Distance traveled (in pixels)', 'Tolerance']
writer = csv.DictWriter(output_csv, delimiter=',', lineterminator='\n', fieldnames=headers)
if output_csv.tell() == 0:
    writer.writeheader()
writeResults = True
########
WIDTH = 1280
HEIGHT = 720
# cap = cv2.imread("ImagesJPG/1280x720.jpg")
walletPNG = cv2.imread("ImagesPNG/wallet-transparent1111x455.png", cv2.IMREAD_UNCHANGED)
walletPNG = cv2.resize(walletPNG, (1155, 360), interpolation=cv2.INTER_AREA)
colorR = (35, 35, 129)
circleList = []
CLOSED_LENGTH = 60
SCORE_ONE_TWO_SEC = 60
SCORE_THREE_SEC = 40
SCORE_FOUR_SEC = 20
RED = (0, 0, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
PURPLE = (255, 0, 255)
TOLERANCE = 1.5

detector = HandDetector(maxHands=1)
end_test_time = 0

class DragCircle:
    def __init__(self, posCenter, size=None):
        if size is None:
            size = [100, 100]
        self.posCenter = posCenter
        self.size = size

    def update(self, cursor):
        oy = self.posCenter[1]
        if (not cursor[0] < 150) and (not cursor[0] > WIDTH - 150):
            self.posCenter = cursor[0], oy

    def isZipGrabbed(self, cursor):
        ox, oy = self.posCenter
        h, w = self.size

        # Check if in region
        if ox - TOLERANCE * w // 2 < cursor[0] < ox + TOLERANCE * w // 2 \
                and oy - TOLERANCE * h // 2 < cursor[1] < oy + TOLERANCE * h // 2:
            return True
            # print("here")
            # if (not cursor[0] < 150) and (not cursor[0] > WIDTH - 150):
            #    self.posCenter = cursor[0], oy
        return False

    def isCursorWithinZipYRange(self, cursor):
        ox, oy = self.posCenter
        h, w = self.size

        if oy - h // 2 < cursor[1] < oy + h // 2:
            return True

        return False


class FingerDraw:
    def __init__(self):
        self.points = []  # all points of the snake
        self.lengths = []  # distance between each point
        self.currentLength = 0  # total length of the snake
        self.allowedLength = 200  # total allowed Length
        self.previousHead = 0, 0  # previous head point

    def update(self, imgMain, currentHead):
        px, py = self.previousHead
        cx, cy = currentHead

        self.points.append([cx, cy])
        distance = math.hypot(cx - px, cy - py)
        self.lengths.append(distance)
        self.currentLength += distance
        self.previousHead = cx, cy

        # Length Reduction
        if self.currentLength > self.allowedLength:
            for i, length in enumerate(self.lengths):
                self.currentLength -= length
                self.lengths.pop(i)
                self.points.pop(i)
                if self.currentLength < self.allowedLength:
                    break

        # Draw Snake
        if self.points:
            for i, point in enumerate(self.points):
                if i != 0:
                    cv2.line(imgMain, self.points[i - 1], self.points[i], (0, 0, 255), 20)
            cv2.circle(imgMain, self.points[-1], 20, (200, 0, 200), cv2.FILLED)

        # Check for Collision
        pts = np.array(self.points[:-2], np.int32)
        pts = pts.reshape((-1, 1, 2))
        cv2.polylines(imgMain, [pts], False, (0, 200, 0), 3)

        return imgMain


isWithinYRange = False
triesToGrab = False


def start(camera_id, tolerance):
    global TOLERANCE
    TOLERANCE = tolerance
    def endTest(score):
        utils.putTextRect(img, "Test Over", [300, 300], colorR=colorR,
                          scale=6, thickness=5, offset=20)
        if score == 0 or score == 1:
            utils.putTextRect(img, "Failed", [300, 450], colorR=colorR,
                              scale=6, thickness=5, offset=20)
        else:
            utils.putTextRect(img, "Success", [300, 450], colorR=colorR,
                              scale=6, thickness=5, offset=20)
        utils.putTextRect(img, f'Score: {score}', [300, 600], colorR=colorR,
                          scale=6, thickness=5, offset=20)

        global writeResults, end_test_time
        if writeResults:
            end_test_time = time.time()
            writer.writerow({'Time': _time, 'Score': score,
                             'Distance traveled (in pixels)': circleList[0].posCenter[0] - 150, 'Tolerance': TOLERANCE})
            writeResults = False

        if int(time.time() - end_test_time) >= 5:
            output_csv.close()
            cap.release()
            cv2.destroyAllWindows()
            sys.exit(0)

    if os.name == 'nt':
        cap = cv2.VideoCapture(camera_id, cv2.CAP_DSHOW)
    else:
        cap = cv2.VideoCapture(camera_id, cv2.CAP_V4L2)
    cap.set(3, WIDTH)
    cap.set(4, HEIGHT)
    cap.set(cv2.CAP_PROP_FPS, int(60))

    start_time = time.time()
    game = FingerDraw()
    circleList.append(DragCircle([150, HEIGHT // 2]))
    circleList.append(DragCircle([WIDTH - 150, HEIGHT // 2]))
    _time = -1
    half = False

    global isWithinYRange, triesToGrab, writeResults
    while True:
        success, img = cap.read()
        # img = cap
        img = cv2.flip(img, 1)

        if _time >= SCORE_ONE_TWO_SEC and circleList[0].posCenter[0] > 300:
            endTest(1)
        elif _time >= SCORE_ONE_TWO_SEC:
            endTest(0)
        # collision detection
        # (x2-x1)^2 + (y2-y1)^2 <= {(r1+r2)^2} (=10000) (10000 - offset = 5000)
        elif (circleList[1].posCenter[0] - circleList[0].posCenter[0]) ** 2 + (
                circleList[1].posCenter[1] - circleList[0].posCenter[1]) ** 2 <= 5000:
            if not half:
                circleList[1] = DragCircle([150, HEIGHT // 2])
                half = True
            else:
                if _time <= SCORE_FOUR_SEC:
                    endTest(4)
                elif _time <= SCORE_THREE_SEC:
                    endTest(3)
                else:
                    endTest(2)
        else:
            _time = int(time.time() - start_time)
            hands, img = detector.findHands(img, flipType=False)

            if hands:
                lmList = hands[0]['lmList']
                pointIndex = lmList[8][0:2]
                img = game.update(img, pointIndex)
                length, info = detector.findDistance(lmList[4], lmList[8])
                # print(length)
                if length < CLOSED_LENGTH:
                    cursor = lmList[8]  # index finger tip landmark

                    if not triesToGrab:
                        triesToGrab = circleList[0].isZipGrabbed(cursor)

                    if triesToGrab:
                        isWithinYRange = circleList[0].isCursorWithinZipYRange(cursor)
                        if isWithinYRange:
                            # call the update here
                            circleList[0].update(cursor)
                else:
                    triesToGrab = False
                    isWithinYRange = False

            utils.putTextRect(img, "Sollerman Test - Wallet", [50, 80], colorR=colorR,
                              scale=3, thickness=3, offset=10)
            utils.putTextRect(img, f'Time: {_time}s', [50, 120], colorR=colorR,
                              scale=2, thickness=2, offset=10)

            # Draw Transparency
            imgNew = np.zeros_like(img, np.uint8)

            cv2.line(imgNew, circleList[0].posCenter, circleList[1].posCenter, colorR, thickness=3)
            utils.putTextCircle(imgNew, "A", circleList[0].posCenter, offset=15, colorR=colorR)
            utils.putTextCircle(imgNew, "B", circleList[1].posCenter, offset=15, colorR=BLUE)

            out = img.copy()
            alpha = 0.2
            mask = imgNew.astype(bool)
            out[mask] = cv2.addWeighted(img, alpha, imgNew, 1 - alpha, 0)[mask]
            img = out
            img = utils.overlay_transparent(img, walletPNG, 100, HEIGHT // 2)
            # img = utils.overlayPNG(img, walletPNG, pos=(100, HEIGHT // 2))

        cv2.imshow("Image", img)
        key = cv2.waitKey(1) & 0xFF

        if key == ord('r'):
            circleList.clear()
            circleList.append(DragCircle([150, HEIGHT // 2]))
            circleList.append(DragCircle([WIDTH - 150, HEIGHT // 2]))
            start_time = time.time()
            _time = int(time.time() - start_time)
            writeResults = True
            half = False
            triesToGrab = False
            isWithinYRange = False
        if key == ord('q'):
            break

    cv2.destroyAllWindows()
